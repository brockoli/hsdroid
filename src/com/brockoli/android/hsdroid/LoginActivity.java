/*
   Copyright [2012] [Rob Woods]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package com.brockoli.android.hsdroid;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.brockoli.android.hsdroid.service.RESTService;

public class LoginActivity extends Activity {
	public static final String USERNAME = "username";
	public static final String PASSWORD = "password";
	
	private String mUsername;
	private String mPassword;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
        autoFillLogin();
    }

    /*
     * Pull username and password for SharedPreferences if they were saved
     */
    private void autoFillLogin() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        if (prefs.contains(USERNAME)) {
            EditText eUsername = (EditText) findViewById(R.id.username);
            eUsername.setText(prefs.getString(USERNAME, ""));
        }
        if (prefs.contains(PASSWORD)) {
            EditText ePassword = (EditText) findViewById(R.id.password);
            ePassword.setText(prefs.getString(PASSWORD, ""));
        }  	
    }
    
    /**
     * Clear the password field
     */
    private void clearPassword() {
        EditText ePassword = (EditText) findViewById(R.id.password);
        ePassword.setText("");
    }
        
    /**
     * Handle login button click
     * @param view
     */
    public void onClickLogin(View view) {
    	// Show the progress bar
    	ProgressBar working = (ProgressBar) findViewById(R.id.working);
    	working.setVisibility(View.VISIBLE);
    	
        EditText eUsername = (EditText) findViewById(R.id.username);
        mUsername = eUsername.getEditableText().toString();
        
        EditText ePassword = (EditText) findViewById(R.id.password);
        mPassword = ePassword.getEditableText().toString();  
        
        new AsyncLogin().execute(mUsername, mPassword, getString(R.string.apikey));
        
        CheckBox cb = (CheckBox) findViewById(R.id.chSavePassword);
    	SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
    	SharedPreferences.Editor editor = prefs.edit();
        if (cb.isChecked()) {
        	editor.putString(USERNAME, mUsername);
        	editor.putString(PASSWORD, mPassword);
        	editor.commit();
        } else {
        	editor.remove(USERNAME);
        	editor.remove(PASSWORD);
        	editor.commit();
        }
    }
            
    /**
     * AsyncTask to handle a login request
     * @author brockoli
     *
     */
    private class AsyncLogin extends AsyncTask<String, Void, String> {
    	
    	/**
    	 * Had an idea to use an Intent here to pass the REST parameters to my REST singleton class
    	 * Reasoning is that if I decide later to swap in an IntentService instead I don't have to change
    	 * much on the front end.
    	 */
		@Override
		protected String doInBackground(String... params) {
			Intent intent = new Intent();
			intent.setData(Uri.parse(getString(R.string.rest_method_login)));

			Bundle bundle = new Bundle();
			bundle.putInt(RESTService.EXTRA_HTTP_VERB, RESTService.POST);

			Bundle extras = new Bundle();
			extras.putString("username", params[0]);
			extras.putString("password", params[1]);
			extras.putString("key", params[2]);

			bundle.putBundle(RESTService.EXTRA_PARAMS, extras);
			intent.putExtras(bundle);

			HttpResponse response = RESTService.request(intent);
			if (response != null) {
				String body = null;
				try {
					body = EntityUtils.toString(response.getEntity());
				} catch (ParseException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
				
				return body;				
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(String result) {
			if (result != null) {
				Boolean success = false;
				
				try {
					JSONObject jsonResponse = (JSONObject) new JSONTokener(result).nextValue();
					if (jsonResponse.getString("status").equals("Success")) {
						success = true;
						String sessionId = jsonResponse.getString("token");
						String favTeam = jsonResponse.getString("favteam");
						String membership = jsonResponse.getString("membership");
						RESTService restService = RESTService.instance();
						restService.setSessionId(sessionId);
						restService.setFavTeam(favTeam);
						restService.setMembership(membership);
					} else {
						clearPassword();
						Toast.makeText(LoginActivity.this, result, Toast.LENGTH_SHORT).show();
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
				
				// Hide the progress bar
				ProgressBar working = (ProgressBar) LoginActivity.this.findViewById(R.id.working);
				working.setVisibility(View.INVISIBLE);
				
				if (success) {
					Intent intent = new Intent(LoginActivity.this, MainActivity.class);
					startActivity(intent);
				}				
			}
		}
    }
}
