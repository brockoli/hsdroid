package com.brockoli.android.hsdroid.provider;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

import com.brockoli.android.hsdroid.provider.HSDroidContract.Notifications;
import com.brockoli.android.hsdroid.provider.HSDroidContract.Teams;

public class HSDroidProvider extends ContentProvider {
	private static final String TAG = HSDroidProvider.class.getSimpleName();

	/**
	 * The database that the provider uses as its underlying data store
	 */
	public static final String DATABASE_NAME = "hsdroid.db";

	/**
	 * The database version
	 */
	private static final int DATABASE_VERSION = 2;
	// Handle to a new DatabaseHelper.
	private DatabaseHelper mOpenHelper;

	/**
	 * A projection map used to select columns from the database
	 */
	private static Map<String, String> sTeamsProjectionMap;

	/**
	 * A projection map used to select columns from the database
	 */
	private static Map<String, String> sNotificationsProjectionMap;

	/**
	 * A UriMatcher instance
	 */
	private static final UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

	// The incoming URI matches the Teams URI pattern
	private static final int TEAMS = 1;

	// The incoming URI matches the Teams ID URI pattern
	private static final int TEAMS_ID = 2;

	// The incoming URI matches the Notifications URI pattern
	private static final int NOTIFICATIONS = 3;

	// The incoming URI matches the Notifications ID URI pattern
	private static final int NOTIFICATIONS_ID = 4;

	/**
	 * A block that instantiates and sets Teams related static objects
	 */
	static {
		// Add a pattern that routes URIs terminated with "teams" to a Teams operation
		sUriMatcher.addURI(HSDroidContract.AUTHORITY, "teams", TEAMS);

		// Add a pattern that routes URIs terminated with "teams" plus an integer to a teams ID operation
		sUriMatcher.addURI(HSDroidContract.AUTHORITY, "teams/#", TEAMS_ID);

		// Initializes a projection map for Teams queries
		sTeamsProjectionMap = new ProjectionBuilder() //
		// Add Teams mappings
		.setTable(Teams.TABLE_NAME) //
		.add(Teams._ID) //
		.add(Teams.COLUMN_NAME_TEAM_NAME) //
		.add(Teams.COLUMN_NAME_CREATE_DATE) //
		.add(Teams.COLUMN_NAME_MODIFICATION_DATE) //
		.build();
	}

	/**
	 * A block that instantiates and sets Notifications related static objects
	 */
	static {
		// Add a pattern that routes URIs terminated with "notifications" to a Teams operation
		sUriMatcher.addURI(HSDroidContract.AUTHORITY, "notifications", NOTIFICATIONS);

		// Add a pattern that routes URIs terminated with "notifications" plus an integer to a notifications ID operation
		sUriMatcher.addURI(HSDroidContract.AUTHORITY, "notifications/#", NOTIFICATIONS_ID);

		// Initializes a projection map for Notifications queries
		sNotificationsProjectionMap = new ProjectionBuilder() //
		// Add Notifications mappings
		.setTable(Notifications.TABLE_NAME) //
		.add(Notifications._ID) //
		.add(Notifications.COLUMN_NAME_NOTIFICATION_ID) //
		.add(Notifications.COLUMN_NAME_CREATE_DATE) //
		.add(Notifications.COLUMN_NAME_MODIFICATION_DATE) //
		.build();
	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		// Opens the database object in "write" mode.
		SQLiteDatabase db = mOpenHelper.getWritableDatabase();
		String finalWhere;

		List<Uri> updatedUris = new ArrayList<Uri>();
		updatedUris.add(uri);

		int count;

		// Does the delete based on the incoming URI pattern.
		switch (sUriMatcher.match(uri)) {

		// If the incoming pattern matches the general pattern for teams, does a delete based on the incoming
		// "where" columns and arguments.
		case TEAMS:
			count = db.delete(HSDroidContract.Teams.TABLE_NAME, selection, // The incoming where clause column
					// names
					selectionArgs // The incoming where clause values
					);
			break;

			// If the incoming URI matches a single team ID, does the delete based on the incoming data, but
			// modifies the where clause to restrict it to the particular team ID.
		case TEAMS_ID:
			/*
			 * Starts a final WHERE clause by restricting it to the desired team ID.
			 */
			finalWhere = HSDroidContract.Teams._ID + // The ID column
			// name
			" = " + // test for equality
			uri.getPathSegments(). // the incoming team ID
			get(HSDroidContract.Teams.TEAMS_ID_PATH_POSITION);

			// If there were additional selection criteria, append them to the final WHERE clause
			if (selection != null) {
				finalWhere = finalWhere + " AND " + selection;
			}

			// Performs the delete.
			count = db.delete(HSDroidContract.Teams.TABLE_NAME, finalWhere, // The final WHERE clause
					selectionArgs // The incoming where clause values.
					);

			if (count > 0) {
				updatedUris.add(Teams.CONTENT_URI);
			}
			break;
			
			// If the incoming pattern matches the general pattern for notifications, does a delete based on the incoming
			// "where" columns and arguments.
			case NOTIFICATIONS:
				count = db.delete(HSDroidContract.Notifications.TABLE_NAME, selection, // The incoming where clause column
						// names
						selectionArgs // The incoming where clause values
						);
				break;

				// If the incoming URI matches a single notifications ID, does the delete based on the incoming data, but
				// modifies the where clause to restrict it to the particular notifications ID.
			case NOTIFICATIONS_ID:
				/*
				 * Starts a final WHERE clause by restricting it to the desired notifications ID.
				 */
				finalWhere = HSDroidContract.Notifications._ID + // The ID column
				// name
				" = " + // test for equality
				uri.getPathSegments(). // the incoming notifications ID
				get(HSDroidContract.Notifications.NOTIFICATIONS_ID_PATH_POSITION);

				// If there were additional selection criteria, append them to the final WHERE clause
				if (selection != null) {
					finalWhere = finalWhere + " AND " + selection;
				}

				// Performs the delete.
				count = db.delete(HSDroidContract.Notifications.TABLE_NAME, finalWhere, // The final WHERE clause
						selectionArgs // The incoming where clause values.
						);

				if (count > 0) {
					updatedUris.add(Notifications.CONTENT_URI);
				}
				break;
		default:
			throw new IllegalArgumentException("Unknown URI " + uri);
		}

		/*
		 * Gets a handle to the content resolver object for the current context, and notifies it that URIs have changed.
		 * The object passes this along to the resolver framework, and observers that have registered themselves for the
		 * provider are notified.
		 */
		ContentResolver resolver = getContext().getContentResolver();
		if (count > 0) {
			for (Uri updatedUri : updatedUris) {
				resolver.notifyChange(updatedUri, null);
			}
		}

		// Returns the number of rows deleted.
		return count;
	}

	@Override
	public String getType(Uri uri) {

		/**
		 * Chooses the MIME type based on the incoming URI pattern
		 */
		switch (sUriMatcher.match(uri)) {
		// If the pattern is for teams, returns the general content type.
		case TEAMS:
			return HSDroidContract.Teams.CONTENT_TYPE;

			// If the pattern is for team IDs, returns the team ID content type.
		case TEAMS_ID:
			return HSDroidContract.Teams.CONTENT_ITEM_TYPE;
			
		case NOTIFICATIONS:
			return HSDroidContract.Notifications.CONTENT_TYPE;

			// If the pattern is for team IDs, returns the notification ID content type.
		case NOTIFICATIONS_ID:
			return HSDroidContract.Notifications.CONTENT_ITEM_TYPE;
			
		default:
			// If the URI pattern doesn't match any permitted patterns, throws an exception.
			throw new IllegalArgumentException("Unknown URI " + uri);
		}
	}

	@Override
	public Uri insert(Uri uri, ContentValues initialValues) {
		// Validates the incoming URI. Only the full provider URI is allowed for inserts.
		if ((sUriMatcher.match(uri) == TEAMS) || (sUriMatcher.match(uri) == NOTIFICATIONS)) {

			// URIs with data that may be influenced by inserts
			List<Uri> affectedUris = new ArrayList<Uri>();
			affectedUris.add(uri);

			// A map to hold the new record's values.
			ContentValues values;

			// If the incoming values map is not null, uses it for the new values.
			if (initialValues != null) {
				values = new ContentValues(initialValues);
			} else {
				// Otherwise, create a new value map
				values = new ContentValues();
			}

			// Gets the current system time in milliseconds
			Long now = Long.valueOf(System.currentTimeMillis());

			// Set the table to insert into and the column to use for the column hack and the base URI
			String table;
			String colHack;
			Uri base;

			switch (sUriMatcher.match(uri)) {
			case TEAMS:
				table = HSDroidContract.Teams.TABLE_NAME;
				colHack = HSDroidContract.Teams.COLUMN_NAME_TEAM_NAME;
				base = HSDroidContract.Teams.CONTENT_ID_URI_BASE;

				// If the values map doesn't contain the creation date, sets the value to the current time.
				if (values.containsKey(HSDroidContract.Teams.COLUMN_NAME_CREATE_DATE) == false) {
					values.put(HSDroidContract.Teams.COLUMN_NAME_CREATE_DATE, now);
				}
				// If the values map doesn't contain the modification date, sets the value to the current time.
				if (values.containsKey(HSDroidContract.Teams.COLUMN_NAME_MODIFICATION_DATE) == false) {
					values.put(HSDroidContract.Teams.COLUMN_NAME_MODIFICATION_DATE, now);
				}
				break;

			case NOTIFICATIONS:
				table = HSDroidContract.Notifications.TABLE_NAME;
				colHack = HSDroidContract.Notifications.COLUMN_NAME_NOTIFICATION_ID;
				base = HSDroidContract.Notifications.CONTENT_ID_URI_BASE;

				// If the values map doesn't contain the creation date, sets the value to the current time.
				if (values.containsKey(HSDroidContract.Notifications.COLUMN_NAME_CREATE_DATE) == false) {
					values.put(HSDroidContract.Notifications.COLUMN_NAME_CREATE_DATE, now);
				}
				// If the values map doesn't contain the modification date, sets the value to the current time.
				if (values.containsKey(HSDroidContract.Notifications.COLUMN_NAME_MODIFICATION_DATE) == false) {
					values.put(HSDroidContract.Notifications.COLUMN_NAME_MODIFICATION_DATE, now);
				}
				break;

			default:
				throw new IllegalArgumentException("Unknown URI " + uri);
			}

			// Opens the database object in "write" mode.
			SQLiteDatabase db = mOpenHelper.getWritableDatabase();

			// Performs the insert and returns the ID of the new record.
			long rowId = db.insertOrThrow(table, // The table to insert into.
					colHack, // A hack, SQLite sets this column value to null if values is empty.
					values // A map of column names, and the values to insert into the columns.
					);

			// If the insert succeeded, the row ID exists.
			if (rowId > 0) {
				// Creates a URI with the record ID pattern and the new row ID appended to it.
				Uri insertedUri = ContentUris.withAppendedId(base, rowId);

				// Notifies observers registered against this provider that the data changed.
				ContentResolver resolver = getContext().getContentResolver();
				resolver.notifyChange(insertedUri, null);

				for (Uri affectedUri : affectedUris) {
					resolver.notifyChange(affectedUri, null);
				}

				return insertedUri;
			}
			// If the insert didn't succeed, then the rowID is <= 0. Throws an exception.
			throw new SQLException("Failed to insert row into " + uri);
		} else {
			throw new IllegalArgumentException("Unknown URI " + uri);
		}
	}

	@Override
	public boolean onCreate() {
		// Creates a new helper object. Note that the database itself isn't opened until something tries to access it,
		// and it's only created if it doesn't already exist.
		mOpenHelper = new DatabaseHelper(getContext());

		// Assumes that any failures will be reported by a thrown exception.
		return true;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		// Constructs a new query builder and sets its table name
		SQLiteQueryBuilder qb = new SQLiteQueryBuilder();

		String groupBy = null;
		String orderBy;

		/**
		 * Choose the projection and adjust the "where" clause based on URI pattern-matching.
		 */
		switch (sUriMatcher.match(uri)) {
		case TEAMS:
			qb.setTables(HSDroidContract.Teams.TABLE_NAME);
			qb.setProjectionMap(sTeamsProjectionMap);

			// set order if specified
			orderBy = setSortOrder(sortOrder, HSDroidContract.Teams.DEFAULT_SORT_ORDER);

			break;

			/*
			 * If the incoming URI is for a single team identified by its ID, chooses the team ID
			 * projection, and appends "_ID = <team>" to the where clause, so that it selects that single team
			 */
		case TEAMS_ID:
			qb.setProjectionMap(sTeamsProjectionMap);
			qb.appendWhere(HSDroidContract.Teams._ID + // the name of the ID column
					"=" +
					// the position of the team ID itself in the incoming URI
					uri.getPathSegments().get(HSDroidContract.Teams.TEAMS_ID_PATH_POSITION));

			// set order if specified
			orderBy = setSortOrder(sortOrder, HSDroidContract.Teams.DEFAULT_SORT_ORDER);

			break;

		case NOTIFICATIONS:
			qb.setTables(HSDroidContract.Notifications.TABLE_NAME);
			qb.setProjectionMap(sNotificationsProjectionMap);

			// set order if specified
			orderBy = setSortOrder(sortOrder, HSDroidContract.Notifications.DEFAULT_SORT_ORDER);

			break;

			/*
			 * If the incoming URI is for a single notification identified by its ID, chooses the notification ID
			 * projection, and appends "_ID = <notifications>" to the where clause, so that it selects that single notifications
			 */
		case NOTIFICATIONS_ID:
			qb.setProjectionMap(sNotificationsProjectionMap);
			qb.appendWhere(HSDroidContract.Notifications._ID + // the name of the ID column
					"=" +
					// the position of the notifications ID itself in the incoming URI
					uri.getPathSegments().get(HSDroidContract.Notifications.NOTIFICATIONS_ID_PATH_POSITION));

			// set order if specified
			orderBy = setSortOrder(sortOrder, HSDroidContract.Notifications.DEFAULT_SORT_ORDER);

			break;

		default:
			// If the URI doesn't match any of the known patterns, throw an exception.
			throw new IllegalArgumentException("Unknown URI " + uri);
		}

		// Opens the database object in "read" mode, since no writes need to be done.
		SQLiteDatabase db = mOpenHelper.getReadableDatabase();

		/*
		 * Performs the query. If no problems occur trying to read the database, then a Cursor object is returned;
		 * otherwise, the cursor variable contains null. If no records were selected, then the Cursor object is empty,
		 * and Cursor.getCount() returns 0.
		 */
		Cursor c = qb.query(db, // The database to query
				projection, // The columns to return from the query
				selection, // The columns for the where clause
				selectionArgs, // The values for the where clause
				groupBy, //
				null, // don't filter by row groups
				orderBy // The sort order
				);

		// Tells the Cursor what URI to watch, so it knows when its source data changes
		c.setNotificationUri(getContext().getContentResolver(), uri);
		return c;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		// Opens the database object in "write" mode.
		SQLiteDatabase db = mOpenHelper.getWritableDatabase();
		int count;
		String finalWhere;

		List<Uri> updatedUris = new ArrayList<Uri>();
		updatedUris.add(uri);

		// Does the update based on the incoming URI pattern
		switch (sUriMatcher.match(uri)) {

		case TEAMS:

			// Does the update and returns the number of rows updated.
			count = db.update(HSDroidContract.Teams.TABLE_NAME, // The database table name.
					values, // A map of column names and new values to use.
					selection, // The where clause column names.
					selectionArgs // The where clause column values to select on.
					);
			break;

			// If the incoming URI matches a single team ID, does the update based on the incoming data, but
			// modifies the where clause to restrict it to the particular team ID.
		case TEAMS_ID:
			/*
			 * Starts creating the final WHERE clause by restricting it to the incoming team ID.
			 */
			finalWhere = HSDroidContract.Teams._ID + // The ID column name
			" = " + // test for equality
			uri.getPathSegments(). // the incoming team ID
			get(HSDroidContract.Teams.TEAMS_ID_PATH_POSITION);

			// If there were additional selection criteria, append them to the final WHERE clause
			if (selection != null) {
				finalWhere = finalWhere + " AND " + selection;
			}

			// Does the update and returns the number of rows updated.
			count = db.update(HSDroidContract.Teams.TABLE_NAME, // The database table name.
					values, // A map of column names and new values to use.
					finalWhere, // The final WHERE clause to use placeholders for whereArgs
					selectionArgs // The where clause column values to select on, or null if the values are in the where
					// argument.
					);
			if (count > 0) {
				updatedUris.add(Teams.CONTENT_URI);
			}
			break;

		case NOTIFICATIONS:

			// Does the update and returns the number of rows updated.
			count = db.update(HSDroidContract.Notifications.TABLE_NAME, // The database table name.
					values, // A map of column names and new values to use.
					selection, // The where clause column names.
					selectionArgs // The where clause column values to select on.
					);
			break;

			// If the incoming URI matches a single notifications ID, does the update based on the incoming data, but
			// modifies the where clause to restrict it to the particular notifications ID.
		case NOTIFICATIONS_ID:
			/*
			 * Starts creating the final WHERE clause by restricting it to the incoming notifications ID.
			 */
			finalWhere = HSDroidContract.Notifications._ID + // The ID column name
			" = " + // test for equality
			uri.getPathSegments(). // the incoming notifications ID
			get(HSDroidContract.Notifications.NOTIFICATIONS_ID_PATH_POSITION);

			// If there were additional selection criteria, append them to the final WHERE clause
			if (selection != null) {
				finalWhere = finalWhere + " AND " + selection;
			}

			// Does the update and returns the number of rows updated.
			count = db.update(HSDroidContract.Notifications.TABLE_NAME, // The database table name.
					values, // A map of column names and new values to use.
					finalWhere, // The final WHERE clause to use placeholders for whereArgs
					selectionArgs // The where clause column values to select on, or null if the values are in the where
					// argument.
					);
			if (count > 0) {
				updatedUris.add(Notifications.CONTENT_URI);
			}
			break;


			// If the incoming pattern is invalid, throws an exception.
		default:
			throw new IllegalArgumentException("Unknown URI " + uri);
		}

		/*
		 * Gets a handle to the content resolver object for the current context, and notifies it that URIs have changed.
		 * The object passes this along to the resolver framework, and observers that have registered themselves for the
		 * provider are notified.
		 */
		ContentResolver resolver = getContext().getContentResolver();
		for (Uri updatedUri : updatedUris) {
			resolver.notifyChange(updatedUri, null);
		}

		// Returns the number of rows updated.
		return count;
	}

	static class DatabaseHelper extends SQLiteOpenHelper {
		public DatabaseHelper(Context context) {
			// calls the super constructor, requesting the default cursor factory.
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			// Create Teams table
			db.execSQL("CREATE TABLE " + HSDroidContract.Teams.TABLE_NAME + " (" //
					+ Teams._ID + " INTEGER PRIMARY KEY," //
					+ Teams.COLUMN_NAME_TEAM_NAME + " TEXT NOT NULL," //
					+ Teams.COLUMN_NAME_CREATE_DATE + " INTEGER," //
					+ Teams.COLUMN_NAME_MODIFICATION_DATE + " INTEGER" + ");");
			// Create Notifications table
			db.execSQL("CREATE TABLE " + HSDroidContract.Notifications.TABLE_NAME + " (" //
					+ Notifications._ID + " INTEGER PRIMARY KEY," //
					+ Notifications.COLUMN_NAME_NOTIFICATION_ID + " INTEGER NOT NULL UNIQUE," //
					+ Notifications.COLUMN_NAME_CREATE_DATE + " INTEGER," //
					+ Notifications.COLUMN_NAME_MODIFICATION_DATE + " INTEGER" + ");");
			}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			// Logs that the database is being upgraded
			Log.w(TAG, "Upgrading database from version " + oldVersion + " to " + newVersion
					+ ", which will destroy all old data");

			// Kills the teams table and existing data
			db.execSQL("DROP TABLE IF EXISTS " + HSDroidContract.Teams.TABLE_NAME);
			db.execSQL("DROP TABLE IF EXISTS " + HSDroidContract.Notifications.TABLE_NAME);
			// Recreates the database with a new version
			onCreate(db);			
		}

		@Override
		public void onDowngrade(SQLiteDatabase db, int oldVersion,
				int newVersion) {
			onUpgrade(db, oldVersion, newVersion);
		}

	}

	private String setSortOrder(String order, String defOrder) {
		// If no sort order is specified, uses the default
		if (TextUtils.isEmpty(order)) {
			return defOrder;
		} else {
			// otherwise, uses the incoming sort order
			return order;
		}
	}

}
