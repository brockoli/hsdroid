package com.brockoli.android.hsdroid.service;

import java.util.ArrayList;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import com.brockoli.android.hsdroid.LoginActivity;
import com.brockoli.android.hsdroid.MainActivity;
import com.brockoli.android.hsdroid.R;
import com.brockoli.android.hsdroid.datamodel.LiveStream;

public class GameTimeAlarm extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		ArrayList<LiveStream> allstreams = intent.getParcelableArrayListExtra(NotificationService.ALL_STREAMS);
		LiveStream livestream = intent.getParcelableExtra(NotificationService.LIVESTREAM);
		
		int count = allstreams.size();
		
		NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
					.setSmallIcon(R.drawable.ic_stat_hs_logo)
					.setContentTitle(context.getString(R.string.game_time_alert))
					.setContentText(livestream.getAwayTeam() + " VS. " + livestream.getHomeTeam() + " " + livestream.getStartTime())
					.setNumber(count)
					.setAutoCancel(true);
		
		NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
		inboxStyle.setBigContentTitle(context.getString(R.string.game_time_alert));
		inboxStyle.setSummaryText(count + context.getString(R.string.fav_teams_playing_today));
		
		for (LiveStream stream : allstreams) {
			inboxStyle.addLine(stream.getAwayTeam() + " VS. " + stream.getHomeTeam() + " " + stream.getStartTime());
		}
		
		mBuilder.setStyle(inboxStyle);
		
		Intent resultIntent = new Intent(context, MainActivity.class);
		
		TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
		stackBuilder.addParentStack(LoginActivity.class);
		stackBuilder.addNextIntent(resultIntent);
		
		PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
		
		mBuilder.setContentIntent(resultPendingIntent);
		NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		mNotificationManager.notify(1000, mBuilder.build());
	}
}
