package com.brockoli.android.hsdroid.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class OnClearNotificationsReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		WakefulIntentService.acquireStaticLock(context);
		Intent i = new Intent(context, NotificationService.class);
		i.putExtra(NotificationService.CLEAR_NOTIFICATIONS, true);
		context.startService(i);
	}

}
