package com.brockoli.android.hsdroid.streams;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.brockoli.android.hsdroid.R;
import com.brockoli.android.hsdroid.datamodel.BaseStream;
import com.brockoli.android.hsdroid.datamodel.LiveStream;
import com.brockoli.android.hsdroid.utils.DBUtils;

public class ExpandableStreamAdapter extends BaseExpandableListAdapter {
    private static final String TAG = ExpandableStreamAdapter.class.getSimpleName();

    private class ViewHolder {
		public ImageView ivEvent;
		public TextView tvHome;
		public TextView tvAway;
		public TextView tvTime;
		public ImageView ivHd;
		public ToggleButton tbFavAway;
		public ToggleButton tbFavHome;
	}

	private StreamsFragment.OnStreamButtonClickedListener mOnStreamButtonClicked;
	private final LayoutInflater mInflater;

	private List mStreams;
	private List mAllStreams;
	private Context mCtx;
	private Boolean mLive;

	public ExpandableStreamAdapter(Context context, List streams, Boolean isLive) {
		this.mInflater = LayoutInflater.from(context);
		this.mStreams = streams;
		this.mAllStreams = streams;
		this.mCtx = context;
		this.mLive = isLive;
	}
	
	public void setSupportedStreams(boolean supported) {
		Log.d(TAG, "Update show supported streams " + supported);
		if (supported) {
			for (int i=0; i < mStreams.size(); i++) {
				if (!((BaseStream) mStreams.get(i)).isIStreamAvailable()) {
					mStreams.remove(i);
				}
			}			
		} else {
			this.mStreams = this.mAllStreams;
		}
	}
	
	public void setOnStreamButtonClickedListener(StreamsFragment.OnStreamButtonClickedListener listener) {
		this.mOnStreamButtonClicked = listener;
	}
	
	// Return the BaseStream object at groupPosition instead of childPosition
	// since we will always have exactly 1 child (the one with the stream options
	// for the selected stream
	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return mStreams.get(groupPosition);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		View resultView = convertView;
		Boolean noStreams = true;
		
		if (resultView == null) {
			resultView = mInflater.inflate(R.layout.stream_select_layout, null);
		}
		
		// Here we will still pass the group and child position even though we will
		// only actually use the groupPosition.  This is in case we change later the 
		// implementation of getChild will only need to change.
		final BaseStream stream = (BaseStream) getChild(groupPosition, childPosition);
		
		if (stream.isIStreamAvailable()) {
			noStreams = false;
			
			Switch hdSwitch = (Switch) resultView.findViewById(R.id.switch_hd);
			hdSwitch.setVisibility(View.VISIBLE);
			hdSwitch.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				
				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					// get the parent
					LinearLayout parent = (LinearLayout) buttonView.getParent();
					
					// set the checked state as a tag on all child views
					for (int i=0; i < parent.getChildCount(); i++) {
						View childView = (View) parent.getChildAt(i);
						childView.setTag(isChecked);
					}
				}
			});
			
			Button btnBeginning = (Button) resultView.findViewById(R.id.btn_stream_beginning);
			btnBeginning.setTag(hdSwitch.isChecked());
			btnBeginning.setVisibility(View.VISIBLE);
			btnBeginning.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					mOnStreamButtonClicked.onStreamButtonClicked(stream, "streams", (Boolean)v.getTag());					
				}
			});
			
			Button btnStream = (Button) resultView.findViewById(R.id.btn_stream_live);
			btnStream.setVisibility(mLive ? View.VISIBLE : View.GONE);
			btnStream.setTag(hdSwitch.isChecked());
			btnStream.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					mOnStreamButtonClicked.onStreamButtonClicked(stream, "nonDVR", (Boolean)v.getTag());
				}
			});
		} else {
			resultView.findViewById(R.id.switch_hd).setVisibility(View.GONE);
			resultView.findViewById(R.id.btn_stream_beginning).setVisibility(View.GONE);
			resultView.findViewById(R.id.btn_stream_live).setVisibility(View.GONE);
		}

		if (noStreams) {
			resultView.findViewById(R.id.no_streams).setVisibility(View.VISIBLE);
		} else {
			resultView.findViewById(R.id.no_streams).setVisibility(View.GONE);
		}
		return resultView;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return 1;
	}

	@Override
	public Object getGroup(int groupPosition) {
		return mStreams.get(groupPosition);
	}

	@Override
	public int getGroupCount() {
		return mStreams.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		View view = convertView;
		ViewHolder holder = new ViewHolder();
		if (convertView == null) {
			view = mInflater.inflate(R.layout.live_stream_item, parent, false);
			holder.ivEvent = (ImageView) view.findViewById(R.id.event);
			holder.tvHome = (TextView) view.findViewById(R.id.home);
			holder.tvAway = (TextView) view.findViewById(R.id.away);
			holder.tvTime = (TextView) view.findViewById(R.id.time);
			holder.ivHd = (ImageView) view.findViewById(R.id.ic_hd);
			holder.tbFavAway = (ToggleButton) view.findViewById(R.id.away_fav);
			holder.tbFavHome = (ToggleButton) view.findViewById(R.id.home_fav);
			view.setTag(holder);
		}
		holder = (ViewHolder) view.getTag();
		
		BaseStream stream = null;
		if (mStreams.get(groupPosition) instanceof LiveStream) {
			stream = (LiveStream) mStreams.get(groupPosition);
			holder.tvTime.setText(((LiveStream) stream).getStartTime());
			holder.tvTime.setVisibility(View.VISIBLE);

			if (stream.isHd()) {
				holder.ivHd.setVisibility(View.VISIBLE);				
			} else {
				holder.ivHd.setVisibility(View.GONE);
			}
		} else if (mStreams.get(groupPosition) instanceof BaseStream) {
			stream = (BaseStream) mStreams.get(groupPosition);
			holder.tvTime.setVisibility(View.GONE);
			holder.ivHd.setVisibility(View.GONE);
		}

		// Setup fields common to BaseStream and LiveStream
		// Setup favorites togglebutton for away team
		holder.ivEvent.setImageResource(getLogoResourceId(stream.getEvent()));
		holder.tvHome.setText(stream.getHomeTeam());
		holder.tvAway.setText(stream.getAwayTeam());
		holder.tbFavAway.setTag(stream.getAwayTeam());
		holder.tbFavAway.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked) {
					DBUtils.insertOrUpdateTeam(mCtx, (String) buttonView.getTag());
				} else {
					DBUtils.deleteTeam(mCtx, (String) buttonView.getTag());
				}
			}
		});
		if (DBUtils.teamExists(mCtx, stream.getAwayTeam())) {
			holder.tbFavAway.setChecked(true);
		} else {
			holder.tbFavAway.setChecked(false);
		}

		// Setup favorites togglebutton for home team
		holder.tbFavHome.setTag(stream.getHomeTeam());
		holder.tbFavHome.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked) {
					DBUtils.insertOrUpdateTeam(mCtx, (String) buttonView.getTag());
				} else {
					DBUtils.deleteTeam(mCtx, (String) buttonView.getTag());
				}
			}
		});
		if (DBUtils.teamExists(mCtx, stream.getHomeTeam())) {
			holder.tbFavHome.setChecked(true);
		} else {
			holder.tbFavHome.setChecked(false);
		}

		return view;
	}

	@Override
	public boolean hasStableIds() {
		return true;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

	private int getLogoResourceId(String event) {
		if (event.equalsIgnoreCase("khl")) return R.drawable.logo_khl;
		if (event.equalsIgnoreCase("nhl")) return R.drawable.logo_nhl;
		if (event.equalsIgnoreCase("ahl")) return R.drawable.logo_ahl;
		if (event.equalsIgnoreCase("ohl")) return R.drawable.logo_ohl;
		if (event.equalsIgnoreCase("qmjhl")) return R.drawable.logo_qmjhl;
		if (event.equalsIgnoreCase("whl")) return R.drawable.logo_whl;
		if (event.equalsIgnoreCase("ufc")) return R.drawable.logo_ufc;
		if (event.equalsIgnoreCase("del")) return R.drawable.logo_del;
		if (event.equalsIgnoreCase("Rookie Tournament")) return R.drawable.logo_iihf;
		if (event.equalsIgnoreCase("World Cup of Hockey")) return R.drawable.logo_iihf;
		if (event.equalsIgnoreCase("World Juniors")) return R.drawable.logo_iihf;
		
		// Default show hockeystreams logo if the event is unknown
		return R.drawable.logo_hs;
	}
	
	public ArrayList<BaseStream> getList() {
		return (ArrayList<BaseStream>) this.mStreams;
	}

}
