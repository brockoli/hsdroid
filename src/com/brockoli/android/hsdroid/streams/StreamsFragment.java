/*
   Copyright [2012] [Rob Woods]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package com.brockoli.android.hsdroid.streams;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.ParseException;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.widget.ExpandableListView;
import android.widget.Toast;

import com.brockoli.android.hsdroid.LoginActivity;
import com.brockoli.android.hsdroid.R;
import com.brockoli.android.hsdroid.VideoPlayerActivity;
import com.brockoli.android.hsdroid.datamodel.BaseStream;
import com.brockoli.android.hsdroid.datamodel.LiveStream;
import com.brockoli.android.hsdroid.service.RESTService;

/**
 * This is a base class for Stream related fragments.  This includes both Live Streams and On demand streams by date or team
 * 
 * When sub-classing, you will need to implement onCreate and provide a view that at least includes a list
 * 
 * To get the list of events, you'll need to sub-class StreamsAsyncTask and implement the onPostExecute method
 * to handle parsing the response JSON object differently depending on if you are requesting live or on demand streams
 * 
 * To play a stream, simple create an instance of StreamAsyncTask and provide an Intent to stream
 * 
 * @author brockoli
 *
 */
public class StreamsFragment extends Fragment {
    private static final String TAG = StreamsFragment.class.getSimpleName();
    
	protected static final String STREAM_URL = "STREAM_ID";
	protected static final String STREAM_TYPE = "STREAM_TYPE";
	protected static final String STREAM_QUALITY = "STREAM_QUALITY";
	protected static final String PREVIOUS_LIST_DATA = "PREVIOUS_LIST_DATA";
	protected static final String LAST_EXPANDED_GROUP_ID = "LAST_EXPANDED_GROUP_ID";

	private ProgressDialog loadingDialog;

	protected ExpandableListView mLv;
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if (mLv.getAdapter() != null) {
			outState.putParcelableArrayList(PREVIOUS_LIST_DATA, ((ExpandableStreamAdapter) mLv.getExpandableListAdapter()).getList());			
		}
	}


	/**
	 * Method to play a stream given it's url
	 * 
	 * @param src
	 */
	protected void playStream(String src) {
		Intent intent = new Intent(getActivity(), VideoPlayerActivity.class);
		intent.putExtra(STREAM_URL, src);
		startActivity(intent);	
	}

	/**
	 * AsyncTask that calls the REST service to get the list of live games
	 * @author brockoli
	 *
	 */
	protected class StreamsAsyncTask extends AsyncTask<Intent, Void, String> {

		@Override
		protected String doInBackground(Intent... params) {
			String body = null;
			HttpResponse response = RESTService.request(params[0]);
			if (response != null && response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				try {
					body = EntityUtils.toString(response.getEntity());
				} catch (ParseException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}				
			}

			return body;
		}
	}

	/**
	 * AsyncTask that calls the REST service to get the streams for a game
	 * @author brockoli
	 *
	 */
	protected class StreamAsyncTask extends AsyncTask<Intent, Void, String> {

		String streamType;
		Boolean isHd;
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			if (loadingDialog == null) {
				loadingDialog = new ProgressDialog(getActivity());
				loadingDialog.setMessage(getString(R.string.loading_stream));
				loadingDialog.isIndeterminate();
				loadingDialog.setCancelable(false);
			}
			loadingDialog.show();
		}

		@Override
		protected String doInBackground(Intent... params) {
			streamType = params[1].getStringExtra(STREAM_TYPE);
			isHd = params[1].getBooleanExtra(STREAM_QUALITY, false);
			String body = null;
			HttpResponse response = RESTService.request(params[0]);
			if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				try {
					body = EntityUtils.toString(response.getEntity());
				} catch (ParseException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}				
			}

			return body;
		}

		@Override
		protected void onPostExecute(String result) {
			if (loadingDialog != null && loadingDialog.isShowing()) {
				loadingDialog.hide();
			}
			if (result != null) {
				try {
					JSONObject jsonResponse = (JSONObject) new JSONTokener(result).nextValue();
					if (jsonResponse.getString("status").equals("Success")) {
						Intent intent = new Intent();
						intent.setAction(Intent.ACTION_VIEW);
						intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
						
						if (streamType.equals("nonDVR")) {
							if (isHd) {
								JSONArray jsonStreams;
								try {
									jsonStreams = jsonResponse.getJSONArray("nonDVRHD");
									for (int i=0; i < jsonStreams.length(); i++) {
										JSONObject jsonStream = jsonStreams.getJSONObject(i);
										if (jsonStream.getString("type").equals("iStream")) {
											String src = jsonStream.getString("src");
											intent.setDataAndType(Uri.parse(src), "video/*");
											Log.d(TAG, "nonDVRHD URL = " + src);
											break;
										}
									}
								} catch (JSONException e) {
									// If there was no streams found in the response set our intent to null
									// so we fail the null check later and throw up a toast
									intent = null;
								}
							} else {
								JSONArray jsonStreams;
								try {
									jsonStreams = jsonResponse.getJSONArray("nonDVRSD");
									for (int i=0; i < jsonStreams.length(); i++) {
										JSONObject jsonStream = jsonStreams.getJSONObject(i);
										if (jsonStream.getString("type").equals("iStream")) {
											String src = jsonStream.getString("src");
											intent.setDataAndType(Uri.parse(src), "video/*");
											Log.d(TAG, "nonDVRSD URL = " + src);
											break;
										}
									}								
								} catch (JSONException e) {
									// If there was no streams found in the response set our intent to null
									// so we fail the null check later and throw up a toast
									intent = null;
								}
							}
						} else if (streamType.equals("streams")) {
							if (isHd) {
								JSONArray jsonStreams;
								try {
									jsonStreams = jsonResponse.getJSONArray("HDstreams");
									for (int i=0; i < jsonStreams.length(); i++) {
										JSONObject jsonStream = jsonStreams.getJSONObject(i);
										if (jsonStream.getString("type").equals("iStream")) {
											String src = jsonStream.getString("src");
											intent.setDataAndType(Uri.parse(src), "video/*");
											Log.d(TAG, "HDstreams URL = " + src);
											break;
										}
									}
								} catch (JSONException e) {
									// If there was no streams found in the response set our intent to null
									// so we fail the null check later and throw up a toast
									intent = null;
								}
							} else {
								JSONArray jsonStreams;
								try {
									jsonStreams = jsonResponse.getJSONArray("SDstreams");
									for (int i=0; i < jsonStreams.length(); i++) {
										JSONObject jsonStream = jsonStreams.getJSONObject(i);
										if (jsonStream.getString("type").equals("iStream")) {
											String src = jsonStream.getString("src");
											intent.setDataAndType(Uri.parse(src), "video/*");
											Log.d(TAG, "SDstreams URL = " + src);
											break;
										}
									}								
								} catch (JSONException e) {
									// If there was no streams found in the response set our intent to null
									// so we fail the null check later and throw up a toast
									intent = null;
								}
							}							
						}
						// start the video with the build Intent
						if (intent != null && intent.getData() != null) {
							startActivity(intent);
						} else {
							showStreamNotStartedDialog();
							//Toast.makeText(StreamsFragment.this.getActivity(), "No URL found for selected stream", Toast.LENGTH_SHORT).show();
						}
					} else {
						Toast.makeText(StreamsFragment.this.getActivity(), result, Toast.LENGTH_SHORT).show();
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}					
			}
		}
	}
	
	private void showStreamNotStartedDialog() {
		AlertDialog.Builder builder =  new AlertDialog.Builder(getActivity());
		builder.setMessage(R.string.program_not_started)
			   .setTitle(R.string.program_not_started_title);
		
		builder.setNegativeButton(R.string.btn_ok, new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		
		AlertDialog dialog = builder.create();
		dialog.show();
	}
	
	private void showNoSupportedStreamDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setMessage(R.string.msg_dialog_no_supported_stream)
			   .setTitle(R.string.title_dialog_no_supported_format);
		
		builder.setNegativeButton(R.string.btn_ok, new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				Intent intent = new Intent(getActivity(), LoginActivity.class);
				startActivity(intent);				
			}
		});
		
		AlertDialog dialog = builder.create();
		dialog.show();
	}

	public interface OnStreamButtonClickedListener {
		public void onStreamButtonClicked(BaseStream stream, String type, Boolean isHd);
	}
}
