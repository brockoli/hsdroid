package com.brockoli.android.hsdroid.datamodel;

import android.os.Parcel;
import android.os.Parcelable;

public class Stream implements Parcelable {
	private static final String TAG = Stream.class.getSimpleName();
	private String mType;
	private String mSrc;
	private String mLocation;
	
	public Stream(String type, String src, String location) {
		this.mType = type;
		this.mSrc = src;
		this.mLocation = location;
	}
	
	// Used by Parcelable to create a stream from a parcel
	public Stream(Parcel source) {
		this.mType = source.readString();
		this.mSrc = source.readString();
		this.mLocation = source.readString();
	}
	
	public void setType(String type) {
		this.mType = type;
	}
	
	public String getType() {
		return this.mType;
	}
	
	public void setSrc(String src) {
		this.mSrc = src;
	}
	
	public String getSrc() {
		return this.mSrc;
	}
	
	public void setLocation(String location) {
		this.mLocation = location;
	}
	
	public String getLocation()	{
		return this.mLocation;
	}
	
	@Override
	public int describeContents() {
		return hashCode();
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(mType);
		dest.writeString(mSrc);
		dest.writeString(mLocation);
	}

	public static final Parcelable.Creator<Stream> CREATOR = new Parcelable.Creator<Stream>() {

		@Override
		public Stream createFromParcel(Parcel source) {
			return new Stream(source);
		}

		@Override
		public Stream[] newArray(int size) {
			return new Stream[size];
		}
	};
}
