   Copyright [2012] [Rob Woods]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

# HSDroid README

HSDroid is a native Android client for watching hockeystreams.com live and on demand video streams.
Currently it will only play iStream (HLS) formated streams as that is what is natively supported by 
Android.  Working on a WMV solution.

Requires you to have an active account on http://www.hockeystreams.com
